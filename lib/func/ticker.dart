class Ticker {
  const Ticker();

  Stream<int> tick(int speed) {
    return Stream.periodic(Duration(milliseconds: speed), (x) => x);
  }
}
