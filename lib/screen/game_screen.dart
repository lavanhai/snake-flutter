import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:snake/food_bloc/food_bloc.dart';
import 'package:snake/general/constant.dart';
import 'package:snake/general/food_painter.dart';
import 'package:snake/model/snake_model.dart';
import 'package:snake/snake_bloc/snake_bloc.dart';

import '../general/snack_painter.dart';
import '../model/food_model.dart';

class GameScreen extends StatefulWidget {
  const GameScreen({Key? key}) : super(key: key);

  @override
  State<GameScreen> createState() => _GameScreenState();
}

class _GameScreenState extends State<GameScreen> {
  Snake snake = Snake();
  Food food = Food();
  late final FocusNode _focusNode;

  @override
  void initState() {
    // TODO: implement initState
    _focusNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RawKeyboardListener(
        autofocus: true,
        onKey: (RawKeyEvent event) {
          if (event.isKeyPressed(LogicalKeyboardKey.arrowUp)) {
            context
                .read<SnakeBloc>()
                .add(const SetSnakeDirection(Direction.down));
          } else if (event.isKeyPressed(LogicalKeyboardKey.arrowDown)) {
            context
                .read<SnakeBloc>()
                .add(const SetSnakeDirection(Direction.up));
          } else if (event.isKeyPressed(LogicalKeyboardKey.arrowLeft)) {
            context
                .read<SnakeBloc>()
                .add(const SetSnakeDirection(Direction.left));
          } else if (event.isKeyPressed(LogicalKeyboardKey.arrowRight)) {
            context
                .read<SnakeBloc>()
                .add(const SetSnakeDirection(Direction.right));
          }
        },
        focusNode: _focusNode,
        child: Scaffold(
          appBar: AppBar(title: const Text("snake Game")),
          body: Container(
              height: heightScreen - appBarHeight - 45,
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.blue, width: 5)),
              child: GestureDetector(
                  onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
                  onHorizontalDragUpdate: (details) {
                    if (details.primaryDelta! > 0) {
                      context
                          .read<SnakeBloc>()
                          .add(const SetSnakeDirection(Direction.right));
                    } else if (details.primaryDelta! < 0) {
                      context
                          .read<SnakeBloc>()
                          .add(const SetSnakeDirection(Direction.left));
                    }
                  },
                  onVerticalDragUpdate: (details) {
                    if (details.primaryDelta! > 0) {
                      context
                          .read<SnakeBloc>()
                          .add(const SetSnakeDirection(Direction.up));
                    } else if (details.primaryDelta! < 0) {
                      context
                          .read<SnakeBloc>()
                          .add(const SetSnakeDirection(Direction.down));
                    }
                  },
                  child: Stack(
                    children: [
                      BlocListener<SnakeBloc, SnakeState>(
                          listener: (contextBloc, state) {
                        if (state is SnakeGameOver) {
                          _showDialogStart(state.snake);
                        }
                      }, child: BlocBuilder<SnakeBloc, SnakeState>(
                        builder: (context, state) {
                          if (state is SnakeInitial) {
                            return CustomPaint(
                              painter: SnakePainter(snake),
                              size:
                                  Size.infinite, // thay đổi kích thước màn hình
                            );
                          }
                          if (state is SnakeUpdate) {
                            return CustomPaint(
                              painter: SnakePainter(state.snake),
                              size:
                                  Size.infinite, // thay đổi kích thước màn hình
                            );
                          }
                          if (state is SnakePause) {
                            return CustomPaint(
                              painter: SnakePainter(state.snake),
                              size:
                                  Size.infinite, // thay đổi kích thước màn hình
                            );
                          }
                          if (state is SnakeGameOver) {
                            return CustomPaint(
                              painter: SnakePainter(state.snake),
                              size:
                                  Size.infinite, // thay đổi kích thước màn hình
                            );
                          }
                          return Container();
                        },
                      )),
                      BlocBuilder<FoodBloc, FoodState>(
                        builder: (context, state) {
                          if (state is FoodInitial) {
                            return CustomPaint(
                              painter: FoodPainter(food),
                              size:
                                  Size.infinite, // thay đổi kích thước màn hình
                            );
                          }
                          if (state is UpdateFood) {
                            return CustomPaint(
                              painter: FoodPainter(state.food),
                              size:
                                  Size.infinite, // thay đổi kích thước màn hình
                            );
                          }
                          return Container();
                        },
                      )
                    ],
                  ))),

          // BlocProvider.of<snakeBloc>(context).add(Play(context));
          floatingActionButton: BlocBuilder<SnakeBloc, SnakeState>(
            builder: (context, state) {
              return FloatingActionButton(
                  onPressed: () => {
                        if (state is SnakeUpdate)
                          {
                            context.read<SnakeBloc>().add(PauseSnake()),
                          }
                        else
                          {
                            context.read<SnakeBloc>().add(RunSnake(context)),
                          }
                      },
                  child: Icon(
                      state is SnakeUpdate ? Icons.pause : Icons.play_arrow));
            },
          ),
        ));
  }

  void _resetSnake() {
    context.read<SnakeBloc>().add(ResetSnake());
  }

  void _showDialogStart(Snake snake) async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => AlertDialog(
        title: const Text("Game Over"),
        content: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(children: <TextSpan>[
            const TextSpan(
                text: "Your score is: ", style: TextStyle(color: Colors.black)),
            TextSpan(
                text: (snake.location!.length * 10).toString(),
                style: const TextStyle(
                    color: Colors.cyan,
                    fontSize: 20,
                    fontWeight: FontWeight.w600)),
          ]),
        ),
        actions: [
          ElevatedButton(
            onPressed: () => {Navigator.pop(context), _resetSnake()},
            child: const Text("Play again"),
          )
        ],
      ),
    );
  }
}
