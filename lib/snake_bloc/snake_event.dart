part of 'snake_bloc.dart';

abstract class SnakeEvent extends Equatable {
  const SnakeEvent();
}

class RunSnake extends SnakeEvent {
  final BuildContext context;
  const RunSnake(this.context);
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class PauseSnake extends SnakeEvent {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class SetGameOver extends SnakeEvent {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class SetSnakeDirection extends SnakeEvent {
  final Direction direction;
  const SetSnakeDirection(this.direction);
  @override
  // TODO: implement props
  List<Object?> get props => [direction];
}

class ResetSnake extends SnakeEvent {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class SetRunSnake extends SnakeEvent {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}
