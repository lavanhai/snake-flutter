part of 'snake_bloc.dart';

abstract class SnakeState extends Equatable {
  const SnakeState();
}

class SnakeInitial extends SnakeState {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class SnakeUpdate extends SnakeState {
  final Snake snake;

  const SnakeUpdate(this.snake);

  @override
  // TODO: implement props
  List<Object?> get props => [snake];
}

class SnakePause extends SnakeState {
  final Snake snake;

  const SnakePause(this.snake);

  @override
  // TODO: implement props
  List<Object?> get props => [snake];
}

class SnakeGameOver extends SnakeState {
  final Snake snake;
  const SnakeGameOver(this.snake);

  @override
  // TODO: implement props
  List<Object?> get props => [snake];
}
