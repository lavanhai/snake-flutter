import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../food_bloc/food_bloc.dart';
import '../func/ticker.dart';
import '../general/constant.dart';
import '../model/food_model.dart';
import '../model/snake_model.dart';

part 'snake_event.dart';

part 'snake_state.dart';

class SnakeBloc extends Bloc<SnakeEvent, SnakeState> {
  Snake snake = Snake();
  StreamSubscription<int>? _tickerSubscription;
  final Ticker _ticker = const Ticker();

  SnakeBloc() : super(SnakeInitial()) {
    on<RunSnake>(_onRunSnake);
    on<SetRunSnake>(_onSetRunSnake);
    on<PauseSnake>(_onPauseSnake);
    on<SetGameOver>(_onGameOver);
    on<SetSnakeDirection>(_onChangeDirection);
    on<ResetSnake>(_onResetSnake);
  }

  void _onRunSnake(RunSnake event, Emitter<SnakeState> emit) async {
    _tickerSubscription?.cancel();
    Food food = event.context.read<FoodBloc>().food;
    int speed = snake.speed;
    _tickerSubscription = _ticker.tick(speed).listen((duration) => {
          if (snake.speed != speed)
            {add(RunSnake(event.context))}
          else
            {_move(event, emit, food)}
        });
  }

  void _onResetSnake(ResetSnake event, Emitter<SnakeState> emit) async {
    snake = Snake();
    emit(SnakeInitial());
  }

  void _onChangeDirection(
      SetSnakeDirection event, Emitter<SnakeState> emit) async {
    if (snake.running) {
      switch (event.direction) {
        case Direction.down:
          if (snake.direction == Direction.up) {
            return;
          }
          break;
        case Direction.up:
          if (snake.direction == Direction.down) {
            return;
          }
          break;
        case Direction.right:
          if (snake.direction == Direction.left) {
            return;
          }
          break;
        default:
          if (snake.direction == Direction.right) {
            return;
          }
          break;
      }
      snake.setDirection(event.direction);
      emit(SnakeUpdate(snake));
    }
  }

  void _onSetRunSnake(SetRunSnake event, Emitter<SnakeState> emit) async {
    Snake newSnake = Snake(location: snake.location);
    emit(SnakeUpdate(newSnake));
  }

  void _onPauseSnake(PauseSnake event, Emitter<SnakeState> emit) async {
    _tickerSubscription?.cancel();
    snake.running = false;
    emit(SnakePause(snake));
  }

  void _onGameOver(SetGameOver event, Emitter<SnakeState> emit) async {
    _tickerSubscription?.cancel();
    emit(SnakeGameOver(snake));
  }

  bool _isGameOver(Offset _tail) {
    if (_tail.dx >= widthScreen - snake.step! ||
        _tail.dy >= (heightScreen - appBarHeight - 50) - snake.step! ||
        _tail.dy < snake.step! ||
        _tail.dx < snake.step!) {
      return true;
    }
    return false;
  }

  bool _isEatFood(Offset _tail, Food food) {
    // if ((_tail.dx - food.location.dx).abs() <= 10 &&
    //     (_tail.dy - food.location.dy).abs() <= 10) {
    //   return true;
    // }
    // return false;
    if (_tail.dx == food.location.dx && _tail.dy == food.location.dy) {
      return true;
    }
    return false;
  }

  void _move(RunSnake event, Emitter<SnakeState> emit, Food food) async {
    Offset tail = snake.location!.last;
    BuildContext context = event.context;
    if (_isGameOver(tail)) {
      _tickerSubscription?.cancel();
      add(SetGameOver());
      return;
    }
    if (_isEatFood(tail, food)) {
      context.read<FoodBloc>().add(CreateFood());
      snake.move(true);
    } else {
      snake.move(false);
    }
    add(SetRunSnake());
  }
}
