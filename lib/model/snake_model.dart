import 'package:flutter/material.dart';
import 'package:snake/general/constant.dart';

class Snake {
  Snake(
      {List<Offset>? location,
      double? step,
      Direction? direction,
      Color? color,
      bool? running,
      int? speed}) {
    this.location = location ?? this.location;
    this.step = step ?? this.step;
    this.direction = direction ?? this.direction;
    this.color = color ?? this.color;
    this.running = running ?? this.running;
    this.speed = speed ?? this.speed;
  }

  List<Offset>? location = [const Offset(10, 10), const Offset(20, 10)];
  double? step = 10;
  Direction? direction = Direction.right;
  Color? color = Colors.red;
  bool running = false;
  int speed = 200;

  void setLocation(List<Offset> location) {
    this.location = location;
  }

  void setDirection(Direction direction) {
    this.direction = direction;
  }

  void addPoint(Offset offset) {
    var a = location;
    a?.add(offset);
    location = a;
  }

  void move(bool eat) {
    running = true;
    Offset tail = location!.last;
    switch (direction) {
      case Direction.left:
        location!.add(Offset(tail.dx - 10, tail.dy));
        break;
      case Direction.right:
        location!.add(Offset(tail.dx + 10, tail.dy));
        break;
      case Direction.up:
        location!.add(Offset(tail.dx, tail.dy + 10));
        break;
      default:
        location!.add(Offset(tail.dx, tail.dy - 10));
    }
    if (!eat) {
      location!.removeAt(0);
    } else {
      speed -= 10;
    }
  }
}
