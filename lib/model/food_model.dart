import 'dart:math';

import 'package:flutter/material.dart';

import '../general/constant.dart';

class Food {
  Offset location = const Offset(30, 10);
  Color color = Colors.red;
  double size = 5;

  void random() {
    double dx = Random().nextInt((widthScreen - 30).toInt()).toDouble();
    double dy =
        Random().nextInt((heightScreen - appBarHeight - 50).toInt()).toDouble();
    // int generatedColor = Random().nextInt(Colors.primaries.length);
    // color = Colors.primaries[generatedColor];
    location = Offset(dx - (dx % 10), dy - (dy % 10));
  }
}
