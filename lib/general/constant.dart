import 'package:flutter/material.dart';

enum Direction { left, right, up, down }

enum GameStatus { stop, playing, gameOver }

final double widthScreen =
    MediaQueryData.fromWindow(WidgetsBinding.instance.window).size.width;
final double heightScreen =
    MediaQueryData.fromWindow(WidgetsBinding.instance.window).size.height;
final double appBarHeight = AppBar().preferredSize.height;
