import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:snake/model/snake_model.dart';

class SnakePainter extends CustomPainter {
  final Snake snake;

  SnakePainter(this.snake);

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = snake.color!; // sử dụng paint để thiết lập màu sắc
    for (final offset in snake.location!) {
      canvas.drawCircle(offset, snake.step! / 2,
          paint); // vẽ từng điểm một dưới dạng hình tròn
    }
  }

  @override
  bool shouldRepaint(SnakePainter oldDelegate) {
    return oldDelegate.snake !=
        snake; // cập nhật lại khi danh sách snake thay đổi
  }
}
