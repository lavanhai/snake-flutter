import 'package:flutter/material.dart';

import '../model/food_model.dart';

class FoodPainter extends CustomPainter {
  final Food food;

  FoodPainter(this.food);

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = food.color; // sử dụng paint để thiết lập màu sắc
    canvas.drawCircle(food.location, food.size, paint);
  }

  @override
  bool shouldRepaint(FoodPainter oldDelegate) {
    return oldDelegate.food != food; // cập nhật lại khi danh sách food thay đổi
  }
}
