import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:snake/food_bloc/food_bloc.dart';
import 'package:snake/screen/game_screen.dart';
import 'package:snake/snake_bloc/snake_bloc.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';

Future<void> main() async {
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MultiBlocProvider(providers: [
        BlocProvider<SnakeBloc>(create: (context) => SnakeBloc()),
        BlocProvider<FoodBloc>(create: (context) => FoodBloc()),
      ], child: const GameScreen()),
    );
  }
}
