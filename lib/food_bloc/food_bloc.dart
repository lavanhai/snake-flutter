import 'dart:math';
import 'dart:ui';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:snake/model/food_model.dart';

part 'food_event.dart';

part 'food_state.dart';

class FoodBloc extends Bloc<FoodEvent, FoodState> {
  late Food food = Food();

  FoodBloc() : super(FoodInitial()) {
    on<CreateFood>(_onCreateFood);
  }

  void _onCreateFood(CreateFood event, Emitter<FoodState> emit) async {
    food.random();
    emit(UpdateFood(food));
  }
}
