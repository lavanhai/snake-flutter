part of 'food_bloc.dart';

abstract class FoodState extends Equatable {
  const FoodState();
}

class FoodInitial extends FoodState {
  @override
  List<Object> get props => [];
}

class UpdateFood extends FoodState {
  final Food food;

  const UpdateFood(this.food);

  @override
  List<Object?> get props => [food];
}
